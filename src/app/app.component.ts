import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { HomePage } from '../pages/home/home';

import { IAppState } from "../store/index";
import { Store } from "@ngrx/store";
import * as FeedActions from '../store/actions/feed-actions';
import * as FavoritesActions from '../store/actions/favorites-actions';
import * as LayoutActions from '../store/actions/layout-actions';
import 'rxjs/add/operator/timeoutWith';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/take'
import 'rxjs/add/observable/empty'
import { Categories } from "../shared/utils/index";

@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    rootPage: any = 'home';

    constructor(platform: Platform,
        statusBar: StatusBar,
        splashScreen: SplashScreen,
        private store: Store<IAppState>) {

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            store.dispatch(new FeedActions.GetFeed(Categories.HOME));
            store.dispatch(new FavoritesActions.AppStart());
        });
    }
    
    menuClose(){
        this.store.dispatch(new LayoutActions.CloseMenu());
    }

    menuOpen(){
        this.store.dispatch(new LayoutActions.OpenMenu());
    }    
}

