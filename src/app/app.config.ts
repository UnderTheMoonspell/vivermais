export const appConfig = {
    minMonths: ['Jan','Fev','Mar','Abr','Maio','Jun','Jul','Ago','Set', 'Out','Nov','Dez'],
    messages: {
        serverError: `De momento não é possível obter aceder ao servidor. Tente mais tarde`,
        noFavorites: `Ainda não adicionou favoritos`
    }
}