import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { StoreModule } from '@ngrx/store';
import { MyApp } from './app.component';
import { SharedModule } from "../shared/shared.module";
import { HttpClientModule } from "@angular/common/http";
import { metaReducers, reducers } from '../store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { FeedEffects } from "../store/effects/feed-effects";
import { ArticleEffects } from "../store/effects/article-effects";
import { ComponentsModule } from "../components/components.module";
import { FavoriteEffects } from "../store/effects/favorite-effects";
import { SearchEffects } from "../store/effects/search-effects";


const COMPONENTS = [
    MyApp
]

@NgModule({
  declarations: COMPONENTS,
  imports: [
    BrowserModule,
    HttpClientModule,
    
    IonicModule.forRoot(MyApp),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 19
    }),    
    SharedModule.forRoot(),
    ComponentsModule,
    EffectsModule.forRoot([FeedEffects, ArticleEffects, FavoriteEffects, SearchEffects])

  ],
  bootstrap: [IonicApp],
  entryComponents: COMPONENTS,
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
