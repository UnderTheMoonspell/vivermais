import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from "./home";
import { ComponentsModule } from "../../components/components.module";
// import { NavbarComponent } from "../../components/navbar-component/navbar-component";

const COMPONENTS = [HomePage]

@NgModule({
  declarations: COMPONENTS,
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(HomePage)
  ],
})
export class HomePageModule { }
