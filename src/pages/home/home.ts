import { Component } from '@angular/core';
import { NavController, IonicPage } from "ionic-angular";
import { Store } from "@ngrx/store";
import * as fromRoot from "../../store/index";
import * as fromFeed from "../../store/reducers/";
import * as ArticleActions from '../../store/actions/article-actions';
import * as FeedActions from '../../store/actions/feed-actions';
import * as FavoriteActions from '../../store/actions/favorites-actions';
import { Observable } from "rxjs/Observable";
import { Feed } from "../../shared/models/feed";
import { Article } from "../../shared/models/article";
import { Categories } from "../../shared/utils/index";

@IonicPage({
    name: 'home'
})
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    feed$: Observable<Article[]>;
    error$: Observable<string>;
    category$: Observable<string>;
    loading$: Observable<boolean>;
    loaded$: Observable<boolean>;

    constructor(public navCtrl: NavController, private store: Store<fromRoot.IAppState>) {
        this.feed$ = store.select(fromFeed.getFeedArticles);
        this.error$ = store.select(fromFeed.getFeedLoadingError);
        this.category$ = store.select(fromFeed.getFeedCategory);
        this.loading$ = store.select(fromFeed.isLoading);
        this.loaded$ = store.select(fromFeed.isLoaded);
    }

    ionViewWillEnter(){
        this.category$.take(1).subscribe((category) => {
            if(category === Categories.FAVORITES){
                this.store.dispatch(new FavoriteActions.GetFavorites());
            }
        })
    }

    openArticle($event: Article){
        this.store.dispatch(new ArticleActions.OpenArticle($event));
    }
    
    loadNextArticles(category: string){
        this.store.dispatch(new FeedActions.GetMoreFeed(category));
    }

    removeFavorite(articleId: number) : void{
        this.store.dispatch(new FavoriteActions.RemoveFavorite(articleId));
    }    
}
