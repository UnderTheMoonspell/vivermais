import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from "../../components/components.module";
import { ArticlePage } from "./article";
// import { NavbarComponent } from "../../components/navbar-component/navbar-component";

const COMPONENTS = [ArticlePage]

@NgModule({
  declarations: COMPONENTS,
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ArticlePage)
  ],
})
export class ArticlePageModule { }
