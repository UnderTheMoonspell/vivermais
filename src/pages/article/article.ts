import { Component, Input, SimpleChanges } from '@angular/core';
import { Article } from "../../shared/models/article";
import { IonicPage, NavParams } from "ionic-angular";
import { Store } from "@ngrx/store";
import * as fromRoot from "../../store/index";
import * as fromFeed from '../../store/reducers';
import * as FavoriteActions from '../../store/actions/favorites-actions';
import { Observable } from "rxjs/Observable";

@IonicPage({
    name: 'article'
})
@Component({
    selector: 'article',
    templateUrl: 'article.html'
})
export class ArticlePage {
    article$: Observable<Article>;
    isFavorite$: Observable<boolean>;
    constructor(
        private store: Store<fromRoot.IAppState>, 
        private params: NavParams) {

            this.article$ = store.select(fromFeed.getArticle);
            this.isFavorite$ = store.select(fromFeed.isFavorite)
    }

    addFavorite(article : Article) : void{
        this.store.dispatch(new FavoriteActions.SaveFavorite(article));
    }

    removeFavorite(articleId: number) : void{
        this.store.dispatch(new FavoriteActions.RemoveFavorite(articleId));
    }      
}
