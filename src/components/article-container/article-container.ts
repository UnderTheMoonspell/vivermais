import { Component, Input, SimpleChanges, ChangeDetectionStrategy, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { Article } from "../../shared/models/article";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    selector: 'article-container',
    templateUrl: 'article-container.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArticleContainer {

    @Input() article: Article;
    @Input() isFavorite: boolean;
    @Output() onAddFavorite = new EventEmitter<Article>();
    @Output() onRemoveFavorite = new EventEmitter<number>();
    @ViewChild('content') contentEl: ElementRef;
    sanitizedContentHTML: SafeHtml;
    sanitizedExcerptHTML: SafeHtml;

    constructor(private sanitizer: DomSanitizer) {
    }

    ngAfterViewInit() {
        const fragment = document.createRange().createContextualFragment(this.article.content);
        this.contentEl.nativeElement.appendChild(fragment);
        let iframe = this.contentEl.nativeElement.getElementsByClassName('resizable');
        if(!iframe.length) return;
        iframe[0].addEventListener('load', () => {
            window.dispatchEvent(new Event('load', { bubbles: true }));
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['article']) {
            this.sanitizedExcerptHTML = this.article &&
                this.sanitizer.bypassSecurityTrustHtml(changes['article'].currentValue.excerpt);
        }
    }

    addFavorite(): void {
        this.onAddFavorite.emit(this.article);
    }

    removeFavorite(): void {
        this.onRemoveFavorite.emit(this.article.id);
    }
}
