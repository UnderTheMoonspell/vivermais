import { Component, Input, SimpleChanges, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Article } from "../../shared/models/article";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { appConfig } from '../../app/app.config';

@Component({
    selector: 'home-container',
    templateUrl: 'home-container.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeContainer {

    @Input() articles: Article[];
    @Input() category: string;
    @Input() loading: boolean;
    @Input() loaded: boolean;
    @Output() onClickArticle = new EventEmitter<Article>();
    @Output() onInfiniteScroll = new EventEmitter<string>();
    @Output() onRemoveFavorite = new EventEmitter<number>();
    hasCompleted:boolean;
    noFavoritesMessage : string = appConfig.messages.noFavorites;
    errorMessage: string = appConfig.messages.serverError;
    constructor() {
    }

    ngOnChanges(changes: SimpleChanges){
        if(changes['articles']){
            this.hasCompleted = true;
        }
    }

    goToArticle(article: Article){
        this.onClickArticle.emit(article);
    }

    doInfinite(infiniteScroll){
        this.hasCompleted && this.onInfiniteScroll.emit(this.category);
        this.hasCompleted = false;
        infiniteScroll.complete();
    }

    removeFavorite(articleId: number): void {
        this.onRemoveFavorite.emit(articleId);
    }    
}
