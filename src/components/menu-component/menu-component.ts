import { Component, Input } from '@angular/core';
import { MenuController } from "ionic-angular";
import { Store } from "@ngrx/store";
import * as fromRoot from "../../store/index";
import * as fromFeed from "../../store/reducers/";
import * as FeedActions from '../../store/actions/feed-actions';
import * as SearchActions from '../../store/actions/search-actions';
import * as FavoritesActions from '../../store/actions/favorites-actions';
import { Categories } from "../../shared/utils/index";
import { Observable } from "rxjs/Observable";

@Component({
    selector: 'menu-component',
    templateUrl: 'menu-component.html'
})
export class MenuComponent {
    isMenuOpened$: Observable<boolean>;
    constructor(public menuCtrl: MenuController, private store: Store<fromRoot.IAppState>) {
        this.isMenuOpened$ = store.select(fromFeed.isMenuOpened);
    }

    getHomeCategory(){
        this.store.dispatch(new FeedActions.GetFeed(Categories.HOME));
        this.closeMenu();
    }

    getAlergiasCategory(){
        this.store.dispatch(new FeedActions.GetFeed(Categories.ALERGIAS));
        this.closeMenu();
    }

    getSaudeCategory(){
        this.store.dispatch(new FeedActions.GetFeed(Categories.SAUDEORAL));
        this.closeMenu();
    }

    getDoresCategory(){
        this.store.dispatch(new FeedActions.GetFeed(Categories.DORES));
        this.closeMenu();
    }

    getProblemasCategory(){
        this.store.dispatch(new FeedActions.GetFeed(Categories.PROBSRESP));
        this.closeMenu();
    }

    getPeleCategory(){
        this.store.dispatch(new FeedActions.GetFeed(Categories.PELE));
        this.closeMenu();
    }

    getFavorites(){
        this.store.dispatch(new FavoritesActions.GotoFavorites());
        this.closeMenu();        
    }

    closeMenu(){
        this.menuCtrl.close();      
    }

    search(searchString: string){
        this.store.dispatch(new SearchActions.Search(searchString));
    }
}
