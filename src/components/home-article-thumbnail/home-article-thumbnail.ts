import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { Article } from "../../shared/models/article";

@Component({
    selector: 'home-article-thumbnail',
    templateUrl: 'home-article-thumbnail.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeArticleThumbnail {

    @Input() article: Article;
    constructor() {}

}
