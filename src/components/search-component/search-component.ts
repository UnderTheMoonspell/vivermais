import { Component, Input, EventEmitter, Output, SimpleChanges } from '@angular/core';
import { MenuController } from "ionic-angular";
import { Store } from "@ngrx/store";
import * as fromRoot from "../../store/index";

@Component({
    selector: 'search-component',
    templateUrl: 'search-component.html'
})
export class SearchComponent {
    searchString: string = '';
    @Input() isMenuOpened: boolean;
    @Output() onSearchEnter = new EventEmitter<string>();
    constructor(private store: Store<fromRoot.IAppState>) {

    }

    ngOnChanges(changes: SimpleChanges){
        if(!changes['isMenuOpened'].firstChange){
            this.searchString = '';
        }
    }

    clickEnter(){
        this.onSearchEnter.emit(this.searchString);
    }

}
