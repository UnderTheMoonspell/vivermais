import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'error-component',
    templateUrl: 'error-component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorComponent {

    @Input() message: string;
    constructor() {}

}
