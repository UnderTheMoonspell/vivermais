import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { Article } from "../../shared/models/article";

@Component({
    selector: 'category-article-thumbnail',
    templateUrl: 'category-article-thumbnail.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryArticleThumbnail {

    @Input() article: Article;
    constructor() {}

}
