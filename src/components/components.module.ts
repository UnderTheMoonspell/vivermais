import { NgModule } from '@angular/core';
import { NavbarComponent } from "./navbar-component/navbar-component";
import { HomeArticleThumbnail } from "./home-article-thumbnail/home-article-thumbnail";
import { ArticleContainer } from "./article-container/article-container";
import { HomeContainer } from "./home-container/home-container";
import { BrowserModule } from "@angular/platform-browser";
import { IonicModule } from "ionic-angular";
import { SharedModule } from "../shared/shared.module";
import { MenuComponent } from "./menu-component/menu-component";
import { CategoryArticleThumbnail } from "./category-article-thumbnail/category-article-thumbnail";
import { FavoriteComponent } from "./favorite-component/favorite-component";
import { FavoriteArticle } from "./favorite-article/favorite-article";
import { ErrorComponent } from "./error-component/error-component";
import { SearchComponent } from "./search-component/search-component";

const COMPONENTS = [
    HomeContainer,
    ArticleContainer,
    HomeArticleThumbnail,
    NavbarComponent,
    MenuComponent,
    CategoryArticleThumbnail,
    FavoriteComponent,
    FavoriteArticle,
    ErrorComponent,
    SearchComponent
]

@NgModule({
  declarations: [COMPONENTS],
  imports: [  
    IonicModule,
    SharedModule
  ],
  exports: [COMPONENTS]
})
export class ComponentsModule { }
