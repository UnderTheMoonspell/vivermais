import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { Article } from "../../shared/models/article";

@Component({
    selector: 'favorite-component',
    templateUrl: 'favorite-component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavoriteComponent {

    @Input() isFavorite: boolean;
    @Output() onAddFavorite = new EventEmitter<void>();
    @Output() onRemoveFavorite = new EventEmitter<void>();
    constructor() {}

    // isFavorite() : boolean{
    //     return this.favoriteArticle;
    // }

    addFavorite() : void{
        // this.isFavorite = true;
        this.onAddFavorite.emit();
    }

    removeFavorite() : void{
        // this.isFavorite = false;
        this.onRemoveFavorite.emit();
    }   
}
