import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { Article } from "../../shared/models/article";

@Component({
    selector: 'favorite-article',
    templateUrl: 'favorite-article.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavoriteArticle {

    @Input() article: Article;
    @Output() onRemoveFavorite = new EventEmitter<number>();
    @Output() onClickArticle = new EventEmitter<Article>();
    constructor() {}

    removeFavorite() : void{
        // this.isFavorite = false;
        this.onRemoveFavorite.emit(this.article.id);
    }

    goToArticle(){
        this.onClickArticle.emit(this.article);
    }    
}
