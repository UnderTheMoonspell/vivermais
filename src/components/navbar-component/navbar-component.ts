import { Component, Input } from '@angular/core';

@Component({
    selector: 'navbar-component',
    templateUrl: 'navbar-component.html'
})
export class NavbarComponent {

    @Input() title: string;

    constructor() {
    }
}
