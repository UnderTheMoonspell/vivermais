import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import * as FeedActions from '../actions/feed-actions';
import * as SearchActions from '../actions/search-actions';
import { DataService } from '../../shared/services/data/data.service';
import * as APIConfig from '../../shared/services/api-urls';

import { Feed } from "../../shared/models/feed";
import { App } from "ionic-angular";
import { Store } from "@ngrx/store";
import { IAppState } from "../index";

@Injectable()
export class SearchEffects {

    @Effect({dispatch: false})
    search$ = this.actions$
        .ofType(SearchActions.ActionTypes.SEARCH)
        .map((action: FeedActions.GetFeed) => action.payload)
        .switchMap((searchString: string) => this.dataService.get(APIConfig.buildSearchUrl(searchString))
            .map((response: any) => {
                if (response) {
                    console.log(JSON.stringify(response.searchResults))
                    // this.app.getActiveNav().setRoot('home');
                    // return new FeedActions.GetFeedSuccess(response);
                } else { throw new Error(`Empty feed`) }
            })
            .catch((error) => Observable.of(new FeedActions.GetFeedError(error)))
        );  


    constructor(
        private actions$: Actions,
        private dataService: DataService,
        private app: App,
        private store$: Store<IAppState>
    ) { }
}
