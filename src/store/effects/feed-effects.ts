import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import * as FeedActions from '../actions/feed-actions';
import { DataService } from '../../shared/services/data/data.service';
import * as APIConfig from '../../shared/services/api-urls';
import { StorageService, KEYS } from '../../shared/services/storage/storage.service';
import { Feed } from "../../shared/models/feed";
import { App } from "ionic-angular";
import { Store } from "@ngrx/store";
import { IAppState } from "../index";
import { appConfig } from "../../app/app.config";

@Injectable()
export class FeedEffects {

    @Effect()
    getFeed$ = this.actions$
        .ofType(FeedActions.ActionTypes.GET_FEED)
        .map((action: FeedActions.GetFeed) => action.payload)
        .switchMap((category) => this.dataService.get(APIConfig.buildURL(category))
            .map((response: Feed) => {
                if (response) {
                    this.app.getActiveNav().setRoot('home');
                    return new FeedActions.GetFeedSuccess(response);
                } else { throw new Error(appConfig.messages.serverError) }
            })
            .catch((error) => Observable.of(new FeedActions.GetFeedError(error)))
        );  

    @Effect()
    moreFeed$ = this.actions$
        .ofType(FeedActions.ActionTypes.GET_MORE_FEED)
        .map((action: FeedActions.GetMoreFeed) => action.payload)
        .withLatestFrom(this.store$)
        .switchMap(([category , state]) => {
            return this.dataService.get(APIConfig.buildURL(category, state.feed.feed.currentPage))
            .map((response: Feed) => {
                if (response) {
                    let newFeed = new Feed();
                    newFeed.items = state.feed.feed.feed.items.concat(response.items);
                    //this.app.getActiveNav().setRoot('home');
                    if(response.items.length) return new FeedActions.GetFeedSuccess(newFeed);
                    return new FeedActions.GetMoreFeedEmpty();
                } else { throw new Error(`Empty feed`) }
            })
            .catch((error) => Observable.of(new FeedActions.GetFeedError(error)))
            }
        );

    constructor(
        private actions$: Actions,
        private dataService: DataService,
        private app: App,
        private store$: Store<IAppState>
    ) { }
}
