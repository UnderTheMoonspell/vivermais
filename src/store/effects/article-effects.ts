import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as ArticleActions from '../actions/article-actions';
import { App } from "ionic-angular";
import { ArticlePage } from "../../pages/article/article";

@Injectable()
export class ArticleEffects {
    @Effect({dispatch : false})
    openArticle$ = this.actions$
        .ofType(ArticleActions.ActionTypes.OPEN_ARTICLE)
        .do(() => {
            this.app.getActiveNav().push('article');
        })

    // @Effect()
    // getFeedSuccess$ = this.actions$
    //     .ofType(Feed.ActionTypes.GET_FEED_SUCCESS)
    //     .switchMap(feed => Observable.of(new Feed.GetFeedSuccess(feed))
    //         .catch(error => Observable.of(new Feed.GetFeedSuccess(error)))
    //     );

    constructor(
        private actions$: Actions,
        private app: App
    ) { }
}
