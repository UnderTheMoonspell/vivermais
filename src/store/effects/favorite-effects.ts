import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as Favorites from '../actions/favorites-actions';
import * as Feed from '../actions/feed-actions';
import { StorageService, KEYS } from "../../shared/services/storage/storage.service";
import { Store } from "@ngrx/store";
import { IAppState } from "../index";
import { App } from "ionic-angular";
import { ArticlePage } from "../../pages/article/article";

@Injectable()
export class FavoriteEffects {
    @Effect()
    appStart$ = this.actions$
        .ofType(Favorites.ActionTypes.APP_START)
        .map(() => new Favorites.AppStartFinish(this.storageService.get(KEYS.FAVORITES)))

    @Effect()
    changeFavorites$ = this.actions$
        .ofType(Favorites.ActionTypes.SAVE_FAVORITE, Favorites.ActionTypes.REMOVE_FAVORITE)
        .withLatestFrom(this.store$)
        .map(([action, state]) => {
            this.storageService.set(KEYS.FAVORITES,  state.feed.favorites.articles);
            let currentPage = this.app.getActiveNav().getActive().component;
            if(currentPage === ArticlePage) return {type:"NO_ACTION"};
            return new Favorites.GetFavorites();
        });        

    @Effect()
    gotoFavorites$ = this.actions$
        .ofType(Favorites.ActionTypes.GOTO_FAVORITES)
        .map(() => {
            this.app.getActiveNav().setRoot('home');
            return new Favorites.GetFavorites();
        });        

    @Effect()
    getFavorites$ = this.actions$
        .ofType(Favorites.ActionTypes.GET_FAVORITES)
        .map(() => {
            return new Feed.GetFavoritesFeedSuccess(this.storageService.get(KEYS.FAVORITES));            
        });

    constructor(
        private actions$: Actions,
        private storageService: StorageService,
        private store$: Store<IAppState>,
        private app: App
    ) { }
}
