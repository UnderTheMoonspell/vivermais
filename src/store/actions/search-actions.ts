import { Action } from '@ngrx/store';
import { type } from '../../shared/utils';

export const ActionTypes = {
    SEARCH: type('[Search] search'),
};

export class Search implements Action {
    readonly type = ActionTypes.SEARCH;

    constructor(public payload: string) { }
}

export type Actions =
    | Search;
 