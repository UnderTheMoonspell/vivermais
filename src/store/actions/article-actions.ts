import { Action } from '@ngrx/store';
import { type } from '../../shared/utils';
import { Article } from "../../shared/models/article";

export const ActionTypes = {
    OPEN_ARTICLE: type('[Article] open article')
};

export class OpenArticle implements Action {
    readonly type = ActionTypes.OPEN_ARTICLE;

    constructor(public payload: Article) { }
}


export type Actions =
    | OpenArticle;
