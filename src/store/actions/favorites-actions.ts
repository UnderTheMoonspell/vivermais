import { Action } from '@ngrx/store';
import { type } from '../../shared/utils';
import { Article } from "../../shared/models/article";

export const ActionTypes = {
    APP_START: type('[Favorites] start app'),
    APP_START_FINISH: type('[Favorites] start app finish'),
    SAVE_FAVORITE: type('[Favorites] add favorite'),
    REMOVE_FAVORITE: type('[Favorites] remove favorite'),
    GET_FAVORITES: type('[Favorites] get favorites'),
    GOTO_FAVORITES: type('[Favorites] goto favorites')
};

export class AppStart implements Action {
    readonly type = ActionTypes.APP_START;

    constructor() { }
}

export class AppStartFinish implements Action {
    readonly type = ActionTypes.APP_START_FINISH;

    constructor(public payload?: Article[]) { }
}

export class SaveFavorite implements Action {
    readonly type = ActionTypes.SAVE_FAVORITE;

    constructor(public payload: Article) { }
}

export class RemoveFavorite implements Action {
    readonly type = ActionTypes.REMOVE_FAVORITE;

    constructor(public payload: number) { }
}

export class GetFavorites implements Action {
    readonly type = ActionTypes.GET_FAVORITES;

    constructor() { }
}

export class GotoFavorites implements Action {
    readonly type = ActionTypes.GOTO_FAVORITES;

    constructor() { }
}

export type Actions =
    | AppStart
    | AppStartFinish
    | SaveFavorite
    | RemoveFavorite
    | GotoFavorites
    | GetFavorites;
