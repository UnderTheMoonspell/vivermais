import { Action } from '@ngrx/store';
import { type } from '../../shared/utils';

export const ActionTypes = {
    OPEN_MENU: type('[Layout] open menu'),
    CLOSE_MENU: type('[Layout] close menu'),
};

export class OpenMenu implements Action {
    readonly type = ActionTypes.OPEN_MENU;

    constructor() { }
}

export class CloseMenu implements Action {
    readonly type = ActionTypes.CLOSE_MENU;

    constructor() { }
}

export type Actions =
    | OpenMenu
    | CloseMenu;
 