import { Action } from '@ngrx/store';
import { type } from '../../shared/utils';
import { Feed } from "../../shared/models/feed";
import { Article } from "../../shared/models/article";

export const ActionTypes = {
    GET_FEED: type('[Feed] get Feed'),
    GET_MORE_FEED: type('[Feed] get more Feed'),
    GET_FEED_SUCCESS: type('[Feed] get Feed success'),
    GET_FEED_ERROR: type('[Feed] get Feed error'),
    GET_FEED_ALERGIAS: type('[Feed] get Feed alergias'),
    GET_FEED_SAUDE: type('[Feed] get Feed saude'),
    GET_FEED_DORES: type('[Feed] get Feed dores'),
    GET_FEED_PROBLEMAS_RESPIRATORIOS: type('[Feed] get Feed problemas respiratorios'),
    GET_FEED_PELE: type('[Feed] get Feed pele'),
    GET_MORE_FEED_EMPTY: type('[Feed] get more feed empty'),
    GET_FAVORITES_FEED_SUCCESS: type('[Feed] get favorites feed'),
};

export class GetFeed implements Action {
    readonly type = ActionTypes.GET_FEED;

    constructor(public payload: string) { }
}

export class GetMoreFeed implements Action {
    readonly type = ActionTypes.GET_MORE_FEED;

    constructor(public payload: string) { }
}

export class GetMoreFeedEmpty implements Action {
    readonly type = ActionTypes.GET_MORE_FEED_EMPTY;

    constructor() { }
}

export class GetFeedSuccess implements Action {
    readonly type = ActionTypes.GET_FEED_SUCCESS;

    constructor(public payload: Feed) { }
}

export class GetFavoritesFeedSuccess implements Action {
    readonly type = ActionTypes.GET_FAVORITES_FEED_SUCCESS;

    constructor(public payload: Article[]) { }
}

export class GetFeedError implements Action {
    readonly type = ActionTypes.GET_FEED_ERROR;

    constructor(public payload: string) { }
}

export type Actions =
    | GetFeed
    | GetMoreFeed
    | GetMoreFeedEmpty
    | GetFeedSuccess
    | GetFavoritesFeedSuccess
    | GetFeedError;
