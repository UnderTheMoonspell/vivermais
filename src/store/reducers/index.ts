import { createSelector, createFeatureSelector, combineReducers, ActionReducer } from '@ngrx/store';
import * as fromFeed from './feed-reducer';
import * as fromArticle from './article-reducer';
import * as fromFavorites from './favorites-reducer';
import * as fromLayout from './layout-reducer';
import { Feed } from "../../shared/models/feed";
import { Article } from "../../shared/models/article";
import { deepEquals } from "../../shared/utils/index";


export interface IFeedState {
    feed: fromFeed.State;
    article: fromArticle.State;
    favorites: fromFavorites.State;
    layout: fromLayout.State;
}

const reducers = {
    feed: fromFeed.reducer,
    article: fromArticle.reducer,
    favorites: fromFavorites.reducer,
    layout: fromLayout.reducer
};

export const reducer: ActionReducer<IFeedState> = combineReducers(reducers);

export const selectFeedGeneralState = createFeatureSelector<IFeedState>('feed');

export const selectFeedState = createSelector(
    selectFeedGeneralState,
    (state: IFeedState) => state.feed
);

export const selectArticleState = createSelector(
    selectFeedGeneralState,
    (state: IFeedState) => state.article
);

export const selectFavoritesState = createSelector(
    selectFeedGeneralState,
    (state: IFeedState) => state.favorites
);

export const selectLayoutState = createSelector(
    selectFeedGeneralState,
    (state: IFeedState) => state.layout
);

/** FEED */

export const getFeed = createSelector(selectFeedState, fromFeed.getFeed);

export const getFeedArticles = createSelector(
    getFeed, 
    (feed: Feed) => {
        if(feed) return feed.items;
        return [];
    });

export const getFirst5FromFeed = createSelector(
    getFeedArticles,
    (articles: Article[]) => articles.slice(0,5));

export const getFeedLoadingError = createSelector(selectFeedState, fromFeed.getFeedError);

export const getFeedCategory = createSelector(selectFeedState, fromFeed.getCategory);

export const isLoading = createSelector(selectFeedState, fromFeed.isLoading);

export const isLoaded = createSelector(selectFeedState, fromFeed.isLoaded);


/** FAVORITES */

export const getFavorites = createSelector(selectFavoritesState, fromFavorites.getFavorites);

/** ARTICLE */

export const getArticle = createSelector(selectArticleState, fromArticle.getArticle);


export const isFavorite = createSelector(
    getFavorites, 
    getArticle,
    (favorites : Article[], article: Article) => {
        if(!favorites || !article) return false;
        return favorites.findIndex(favArticle => favArticle.id == article.id) >= 0;
    });

/** MENU */

export const isMenuOpened = createSelector(selectLayoutState, fromLayout.isMenuOpened);