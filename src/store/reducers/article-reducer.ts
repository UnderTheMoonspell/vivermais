import { Actions, ActionTypes } from '../actions/article-actions';
import { CustomAction } from '../../shared/utils';
import { Article } from "../../shared/models/article";

export interface State {
  error?: string;
  article?: Article;
}

const initialState: State = {
  error: ''
};

export function reducer(state: State = initialState, action: CustomAction<State>): State {
  switch (action.type) {
    case ActionTypes.OPEN_ARTICLE:
      return Object.assign({},
        state, {
          article: action.payload
        });
    default:
      return state;
  }
}

export const getArticle = (state: State) => state.article;

