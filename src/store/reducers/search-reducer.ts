import { Actions, ActionTypes } from '../actions/search-actions';
import { CustomAction } from '../../shared/utils';

export interface State {
    searchString: string
}

const initialState: State = {
    searchString: ''
};

export function reducer(state: State = initialState, action: CustomAction<State>): State {
    switch (action.type) {
        case ActionTypes.SEARCH:
            return Object.assign({}, state, { searchString: action.payload });
        default:
            return state;
    }
}


