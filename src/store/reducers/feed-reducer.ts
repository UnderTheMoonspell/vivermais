import { Actions, ActionTypes } from '../actions/feed-actions';
import { CustomAction, Categories } from '../../shared/utils';
import { Feed } from "../../shared/models/feed";
import { Article } from "../../shared/models/article";

export interface State {
    error?: string;
    loaded: boolean;
    loading: boolean;
    feed?: Feed;
    currentPage: number;
    currentCategory?: string;
}

const initialState: State = {
    loaded: false,
    loading: false,
    error: '',
    currentPage: 1
};

export function reducer(state: State = initialState, action: CustomAction<State>): State {
    switch (action.type) {
        case ActionTypes.GET_FEED:
            return Object.assign({},
                state, {
                    loading: true,
                    currentPage: 1,
                    currentCategory: action.payload
                }
            );
        case ActionTypes.GET_FEED_SUCCESS:
            return Object.assign({},
                state, {
                    loaded: true,
                    loading: false,
                    feed: action.payload
                });
        case ActionTypes.GET_MORE_FEED:
            return Object.assign({},
                state, {
                    loading: true,
                    currentCategory: action.payload,
                    currentPage: state.currentPage + 1
                });
        case ActionTypes.GET_MORE_FEED_EMPTY:
            return Object.assign({},
                state, {
                    loading: false,
                    currentPage: state.currentPage
                });
        case ActionTypes.GET_FEED_ERROR:
            return Object.assign({}, state, {
                error: action.payload,
                loaded: true,
                loading: false
            });
        case ActionTypes.GET_FAVORITES_FEED_SUCCESS:
            let favorites = new Feed();
            favorites.items = (action as any).payload;
            return Object.assign({}, state, {
                feed: favorites,
                currentCategory: Categories.FAVORITES
            });
        default:
            return state;
    }
}

export const getFeedError = (state: State) => state.error;

export const getFeed = (state: State) => state.feed;

export const isLoading = (state: State) => state.loading;

export const isLoaded = (state: State) => state.loaded;

export const getCategory = (state: State) => state.currentCategory;
