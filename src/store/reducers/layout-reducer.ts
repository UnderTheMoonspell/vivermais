import { Actions, ActionTypes } from '../actions/layout-actions';
import { CustomAction } from '../../shared/utils';

export interface State {
    isOpened: boolean
}

const initialState: State = {
    isOpened: false
};

export function reducer(state: State = initialState, action: CustomAction<State>): State {
    switch (action.type) {
        case ActionTypes.OPEN_MENU:
            return { ...state, isOpened: true };
        case ActionTypes.CLOSE_MENU:
            return { ...state, isOpened: false };
        default:
            return state;
    }
}

export const isMenuOpened = (state: State) => state.isOpened;

