import { Actions, ActionTypes } from '../actions/favorites-actions';
import { CustomAction } from '../../shared/utils';
import { Article } from "../../shared/models/article";

export interface State {
    articles?: Article[];
}

const initialState: State = {
    articles: []
};

export function reducer(state: State = initialState, action: CustomAction<State>): State {
    switch (action.type) {
        case ActionTypes.APP_START_FINISH:
            if(action.payload)
            return Object.assign({}, state, { articles :  action.payload});
        case ActionTypes.SAVE_FAVORITE: {
            let newFavorite: Article = action.payload as Article;
            if(!newFavorite 
            || (state.articles
                && state.articles.findIndex(favArticle => favArticle.id == newFavorite.id) >= 0)){
                return state;
            }
            return Object.assign({}, state, { articles : [ ...state.articles, newFavorite]});
        }
        case ActionTypes.REMOVE_FAVORITE: {
            let id = action.payload;
            return Object.assign({}, state, { 
                articles : state.articles.filter(favArticles => favArticles.id != id)
            }); 
        }        
        default:
            return state;
    }
}

export const getFavorites = (state: State) => state.articles;

