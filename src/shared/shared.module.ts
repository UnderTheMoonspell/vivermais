import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

// import { LoaderComponent } from './components/loader/loader.component';

import { DataService } from './services/data/data.service';
import { StorageService } from './services/storage/storage.service';
import { PortugueseMonth } from "./pipes/portuguese-month";
import { Category } from "./pipes/category";

const COMPONENTS = [
    PortugueseMonth,
    Category
]

@NgModule({
    declarations: [
        COMPONENTS
    ],
    imports: [
        CommonModule
    ],
    exports: [
        COMPONENTS
    ]
})
export class SharedModule {
    static forRoot() {
        return {
            ngModule: SharedModule,
            providers: [DataService, StorageService]
        }
    }
}
