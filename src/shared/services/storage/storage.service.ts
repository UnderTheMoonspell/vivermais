import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

export const KEYS = {
  FAVORITES: 'FAVORITES_KEY',
}

@Injectable()
export class StorageService {

  constructor() { }

  public get(key: string) : any {
    return JSON.parse(localStorage.getItem(key));
  } 

  public set(key: string, value: any) : void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  public remove(key: string) : void {
    localStorage.removeItem(key);
  }  
}
