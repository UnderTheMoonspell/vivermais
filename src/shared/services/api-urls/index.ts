import { Categories } from "../../utils/index";

const baseURL = 'http://vivermaisemelhor.sapo.pt/feed?json=true';

export const URLS = {
    getFeed: baseURL,
    getFeedAlergias: `${baseURL}&category=alergias`,
    getFeedSaude: `${baseURL}&category=saude-oral`,
    getFeedDores: `${baseURL}&category=dores`,
    getFeedProblemasRespiratorios: `${baseURL}&category=problemas-respiratorios`,
    getFeedPele: `${baseURL}&category=pele`,

    search: `http://vivermaisemelhor.sapo.pt/api/search/`
}


export function buildURL(category: string, page: number = 1): string {
    // if(param){
    //     return `${baseURL}${url}`.replace('%',param);
    // }
    if (category === Categories.HOME) return `${baseURL}&page=${page}`;
    return `${baseURL}&category=${category}&page=${page}`;
}

export function buildSearchUrl(searchString:string){
    return `${URLS.search}${searchString}`;
}