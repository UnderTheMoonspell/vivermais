import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';


@Injectable()
export class DataService {

  constructor(private http: HttpClient) { }

  private _authenticated = false;

  public get(url: string): Observable<any> {
    return this.http.get(url)
      // .timeoutWith(10000, Observable.throw(new Error(`Erro no acesso ao servidor. Tente mais tarde`)))
      .catch((err: HttpErrorResponse) => {
        let error = typeof err.error === 'object' || !err.error ?
          `Erro no acesso ao servidor. Tente mais tarde` : err.error;
        return Observable.throw(error);
      });
  }

  public getCached(url: string, key: string): Observable<any> {
    return this.http.get(url).map(res => {
      localStorage.setItem(key, JSON.stringify(res))
      return res;
    });
  }

  public post(url: string, body: any): Observable<any> {
    return this.http.post(url, body)
      .timeoutWith(10000, Observable.throw(new Error(`Erro no acesso ao servidor. Tente mais tarde`)))
      .catch((err: HttpErrorResponse) => {
        let error = typeof err.error === 'object' || !err.error ?
          `Erro no acesso ao servidor. Tente mais tarde` : err.error;
        return Observable.throw(error);
      });
  }
}
