import { Pipe } from "@angular/core";
import { appConfig } from '../../app/app.config';
import { Categories } from "../utils/index";
@Pipe({
    name: "category"
})

export class Category {
    constructor() { }

    transform(input: string) {
        if (!input) { return ""; }
        switch(input){
            case Categories.ALERGIAS:
                return 'Alergias';
            case Categories.DORES:
                return 'Dores';
            case Categories.PELE:
                return 'Pele';
            case Categories.PROBSRESP:
                return 'Problemas Respiratórios';
            case Categories.SAUDEORAL:
                return 'Saúde Oral';
            case Categories.FAVORITES:
                return 'Favoritos';                
            default:
                return 'Home';
        }
    }
}