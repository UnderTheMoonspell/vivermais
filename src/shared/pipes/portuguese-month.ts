import { Pipe } from "@angular/core";
import * as moment from 'moment';
import { appConfig } from '../../app/app.config';
@Pipe({
    name: "portugueseMonth"
})

export class PortugueseMonth {
    constructor() { }

    transform(input: Date) {
        if (!input) { return ""; }
        let month = parseInt(moment(input).format('MM'));
        let translateMonth = appConfig.minMonths[month - 1];
        return moment(input).format(`DD [${translateMonth}] YYYY`);
    }
}