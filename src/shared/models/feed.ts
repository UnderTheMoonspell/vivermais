import { Article } from "./article";

export class Feed {
    items: Article[];
    items_per_page: number;
    lastUpdate: number;
    start_index: number;
    total_results: number;
}
