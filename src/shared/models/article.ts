export class Article{
    id: number;
    categories: Array<any>[];
    content: string;
    excerpt: string;
    publish_date: Date;
    thumbnail: any;
    title: string;
    slug: string;
}